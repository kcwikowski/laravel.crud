# Cyber-Duck

This is a test assignment application using Laravel framework.

## Installation

Using [Docker](https://www.docker.com/)

```bash
./docker/docker-start.sh
```
Above will configure and start all required services and initialize application.

By default application will only seed the admin user(admin@admin.com)
to seed Company and Employee entities please run following command within container
```bash
php artisan db:seed --class=Setup
```

Add following line to /etc/hosts on Linux or c:/Windows/System32/drivers/etc/hosts on Windows to make the virtual host of the application accessible from web browser
```bash
IP_OF_YOUR_DOCKER_MACHINE cyber-duck.local
```

## Usage

[Cyber-Duck](http://cyber-duck.local:8000)


## Author
[Kamil Ćwikowski](mailto:kcwikowski@gmail.com)