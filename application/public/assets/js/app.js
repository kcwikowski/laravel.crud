Vue.component('unit-item', {
    template: '#unitItemTemplate',
    props   : {
        id      : null,
        name    : null,
        address : null,
        postcode: null,
        charges : null,
        status  : null,
    },
    computed: {
        buttonLabel() {

            switch (this.status) {
                case 'available':
                    return 'start';
                case 'charging':
                    return 'stop';
                default:
                    return 'wait';
            }
        },
        buttonClass() {
            switch (this.status) {
                case 'available':
                    return 'btn-success';
                case 'charging':
                    return 'btn-warning';
                default:
                    return 'btn-secondary disabled';
            }
        },
        chargesCountText() {
            let count = this.charges.length;
            if (0 === count) {
                return 'No charges yet';
            }
            if (count > 1) {
                return count.toString() + ' charges';
            }
            return '1 charge';
        }
    },
    methods : {
        message(text){
            $(CyberDuckApp.$refs.messageModal).find('.modal-title').text(text);
            $(CyberDuckApp.$refs.messageModal).modal();
        },
        actOnUnit() {
            switch (this.status) {
                case 'available':
                    axios.post(['/units', this.id].join('/'))
                        .then(function (response) {
                        })
                        .catch(function (error) {
                            CyberDuckApp.message('Charge not started');
                            console.log(error);
                        })
                        .then(function () {
                            CyberDuckApp.getUnits();
                        });
                    break;
                case 'charging':
                    axios.patch(['/units', this.id].join('/'))
                        .then(function (response) {
                        })
                        .catch(function (error) {
                            CyberDuckApp.message('Charge not stopped');
                            console.log(error);
                        })
                        .then(function () {
                            CyberDuckApp.getUnits();
                        });
                    break;
            }
        }
    }
});
let CyberDuckApp = new Vue({
    el     : '#CyberDuck',
    data   : {
        pageTitle: 'Units',
        user     : null,
        units    : [],
    },
    async created() {
        await this.getUser();
        this.getUnits();
    },
    methods: {
        async getUser() {
            await axios.get('/user')
                .then(function (response) {
                    CyberDuckApp.user = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getUnits() {
            $('.loader-icon').addClass('active');
            axios.get('/units')
                .then(function (response) {
                    CyberDuckApp.units = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $('.loader-icon').removeClass('active');
                });
        },
        changeUser() {
            axios.post('/user', {
                user: this.user
            })
                .then(function (response) {
                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $(CyberDuckApp.$refs.accountModal).modal('hide');
                    CyberDuckApp.getUnits();
                });
        }
    }
})