<?php

use Illuminate\Support\Facades\Route;

Route::get('/', "Index@index")
    ->name('home');
{//Auth
    Route::get('/login', "Authenticate@login")
        ->name("login");

    Route::post('/login', "Authenticate@authenticate");

    Route::get('/logout', "Authenticate@logout")
        ->middleware('auth')
        ->name("logout");
}
Route::resource('/company', "Company");
Route::resource('/employee', "Employee");
Route::post('/company/autocomplete', "Company@autocomplete")
    ->name('company.autocomplete');
