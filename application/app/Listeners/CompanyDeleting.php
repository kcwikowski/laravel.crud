<?php

namespace App\Listeners;

use App\Http\Middleware\CompanyLogo;

class CompanyDeleting
{
    /**
     * @param \App\Events\CompanyDeleting $event
     */
    public function handle(\App\Events\CompanyDeleting $event)
    {
        CompanyLogo::deleteLogo($event->getCompany());
        $event->getCompany()->employees()->delete();
    }
}
