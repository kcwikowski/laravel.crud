<?php

namespace App\Providers;

use App\Company;
use App\Employee;
use App\User;
use Illuminate\Support\ServiceProvider;
use Ramsey\Uuid\Uuid;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Company::creating(function (Company $company) {
            if (!$company->id) {
                $company->id = Uuid::uuid4()->toString();
            }
        });
        Employee::creating(function (Employee $employee) {
            if (!$employee->id) {
                $employee->id = Uuid::uuid4()->toString();
            }
        });
        User::creating(function (User $user) {
            if (!$user->id) {
                $user->id = Uuid::uuid4()->toString();
            }
        });
    }
}
