<?php

namespace App\Events;

use App\Company;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CompanyDeleting
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Company
     */
    protected $company;

    /**
     * CompanyDeleting constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->setCompany($company);
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return static
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }
}
