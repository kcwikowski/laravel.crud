<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public    $incrementing = false;
    protected $table        = 'employee';
    protected $with         = ['company'];
    protected $fillable     = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'company_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Company
     */
    public function company()
    {
        return
            $this->belongsTo('App\Company');
    }
}
