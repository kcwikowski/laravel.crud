<?php

namespace App;

use App\Events\CompanyDeleting;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public    $incrementing     = false;
    protected $table            = 'company';
    protected $fillable         = [
        'name',
        'email',
        'website',
    ];
    protected $dispatchesEvents = [
        'deleting' => CompanyDeleting::class,
    ];

    /**
     * @param $name
     * @param int $limit
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function autocomplete($name, $limit = 20)
    {
        return
            self::query()
                ->select([
                    'id',
                    'name AS label',
                ])
                ->where('name', 'LIKE', $name . '%')
                ->limit($limit)
                ->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Employee
     */
    public function employees()
    {
        return
            $this->hasMany('App\Employee');
    }
}
