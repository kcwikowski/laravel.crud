<?php

namespace App\Http\Middleware;

use App\Company;
use Intervention\Image\Facades\Image;

class CompanyLogo
{
    CONST ORIGINAL_PREFIX  = 'resources/original/';
    CONST THUMBNAIL_PREFIX = 'resources/thumbnail/';
    CONST THUMBNAIL_SIZE   = [64, 64];
    /**
     * @var bool
     */
    protected static $initialized;
    /**
     * @var string
     */
    protected static $originalPrefix;
    /**
     * @var string
     */
    protected static $thumbnailPrefix;

    /**
     * @return bool
     */
    public static function isInitialized(): ?bool
    {
        return self::$initialized;
    }

    /**
     * @param bool $initialized
     */
    public static function setInitialized(bool $initialized)
    {
        self::$initialized = $initialized;
    }

    public static function initialize()
    {
        if (!self::isInitialized()) {
            if (!file_exists(public_path(self::getOriginalPrefix()))) {
                mkdir(public_path(self::getOriginalPrefix()));
            }

            if (!file_exists(public_path(self::getThumbnailPrefix()))) {
                mkdir(public_path(self::getThumbnailPrefix()));
            }

            self::setInitialized(true);
        }
    }

    /**
     * @param Company $company
     * @param bool $asset
     * @return string
     */
    public static function getLogoUrl(Company $company, $asset = true)
    {
        $path = self::getOriginalPrefix() . $company->logo;

        return $asset ? asset($path) : $path;
    }

    /**
     * @param Company $company
     * @return string
     */
    public static function getLogoThumbnailUrl(Company $company, $asset = true)
    {
        $path = self::getThumbnailPrefix() . $company->logo;

        return $asset ? asset($path) : $path;
    }

    /**
     * @param Company $company
     * @return bool
     */
    public static function deleteLogo(Company $company)
    {
        return
            @(
                unlink(public_path(self::getOriginalPrefix() . $company->logo))
                && unlink(public_path(self::getThumbnailPrefix() . $company->logo))
            );
    }

    /**
     * @param Company $company
     * @param string $imagePath
     * @param null|string $extension
     * @return bool
     */
    public static function processLogo(Company $company, string $imagePath, ?string $extension = null)
    {
        self::initialize();
        $logoInfo = new \SplFileInfo($imagePath);

        if (!$logoInfo->isReadable()) {

            return false;
        }

        try {

            $targetPath = $company->id . '.' . ($extension ?: $logoInfo->getExtension());
            $thumbnail  = Image::make($logoInfo->getRealPath());

            $thumbnail
                ->resize(...self::THUMBNAIL_SIZE)
                ->save(public_path(self::getThumbnailPrefix() . $targetPath));
            copy(
                $logoInfo->getRealPath(),
                public_path(self::getOriginalPrefix() . $targetPath)
            );
        } catch (\Exception $e) {

            return false;
        }

        return
            $company
                ->setAttribute('logo', $targetPath)
                ->save();
    }

    /**
     * @return string
     */
    public static function getOriginalPrefix(): string
    {
        return
            is_null(self::$originalPrefix) ?
                self::ORIGINAL_PREFIX :
                self::$originalPrefix;
    }

    /**
     * @param string $originalPrefix
     */
    public static function setOriginalPrefix(string $originalPrefix)
    {
        self::$originalPrefix = $originalPrefix;
    }

    /**
     * @return string
     */
    public static function getThumbnailPrefix(): string
    {
        return
            is_null(self::$thumbnailPrefix) ?
                self::THUMBNAIL_PREFIX :
                self::$thumbnailPrefix;
    }

    /**
     * @param string $thumbnailPrefix
     */
    public static function setThumbnailPrefix(string $thumbnailPrefix)
    {
        self::$thumbnailPrefix = $thumbnailPrefix;
    }
}
