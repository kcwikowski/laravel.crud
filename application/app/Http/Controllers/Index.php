<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class Index extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $companies =
            \App\Company::query()
                ->orderBy('created_at', 'desc')
                ->limit(5)
                ->get();
        $employees =
            \App\Employee::query()
                ->orderBy('created_at', 'desc')
                ->limit(5)
                ->get();

        return
            view(
                'index',
                compact([
                    'companies',
                    'employees',
                ])
            );
    }
}
