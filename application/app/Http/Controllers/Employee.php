<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class Employee extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = \App\Employee::query()->paginate(10);

        return
            view('employee.index', compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return
            $this->edit(new \App\Employee());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Employee $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Employee $request)
    {
        if (($employee = new \App\Employee($request->validated()))->save()) {
            return
                redirect()
                    ->route(
                        'employee.show',
                        $employee,
                        302,
                        [
                            'X-Created' => 1,
                        ]
                    )
                    ->with('success', 'Employee created.');
        }

        $request->flash();

        return
            redirect()
                ->route('employee.create')
                ->with('error', 'Error occurred when saving a new employee');
    }

    /**
     * @param \App\Employee $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(\App\Employee $employee)
    {
        return
            view(
                'employee.show',
                compact('employee')
            );
    }

    /**
     * @param \App\Employee $employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(\App\Employee $employee)
    {
        return
            view(
                'employee.edit',
                compact('employee')
            );
    }

    /**
     * @param \App\Http\Requests\Employee $request
     * @param \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Employee $request, \App\Employee $employee)
    {
        if ($employee->update($request->all())) {
            return
                redirect()
                    ->route(
                        'employee.show',
                        $employee,
                        302,
                        [
                            'X-Updated' => 1,
                        ]
                    )
                    ->with('success', 'Employee updated');
        }

        return
            redirect()
                ->route('employee.edit', $employee)
                ->with('error', 'Employee not updated');
    }

    /**
     * @param \App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Employee $employee)
    {
        if ($employee->delete()) {
            return
                redirect()
                    ->route(
                        'employee.index',
                        [],
                        302,
                        [
                            'X-Deleted' => 1,
                        ]
                    )
                    ->with('success', 'Employee deleted');
        }

        return
            redirect()
                ->route('employee.show', $employee)
                ->with('error', 'Employee not deleted');
    }
}
