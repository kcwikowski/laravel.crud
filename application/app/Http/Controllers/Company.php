<?php

namespace App\Http\Controllers;

use App\Http\Middleware\CompanyLogo;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class Company extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function autocomplete(Request $request)
    {
        $data = [];

        if ($request->has('name')) {
            $data =
                \App\Company::autocomplete(
                    $request->name
                );
        }

        return
            response()->json($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = \App\Company::query()->paginate(10);

        return
            view('company.index', compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return
            $this->edit(new \App\Company());
    }

    /**
     * @param \App\Http\Requests\Company $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Company $request)
    {
        $company = new \App\Company($request->validated());

        if (
            $company->save()
            && CompanyLogo::processLogo(
                $company,
                $request->file('logo')->getRealPath(),
                $request->file('logo')->getClientOriginalExtension()
            )
        ) {
            return
                redirect()
                    ->route(
                        'company.show',
                        $company,
                        302,
                        [
                            'X-Created' => 1,
                        ]
                    )
                    ->with('success', 'Company created.');
        }

        $request->flash();

        return
            redirect()
                ->route('company.create')
                ->with('error', 'Error occurred when saving a new company');
    }

    /**
     * @param \App\Company $company
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(\App\Company $company)
    {

        return
            view(
                'company.show',
                compact('company')
            );
    }

    /**
     * @param \App\Company $company
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(\App\Company $company)
    {
        return
            view(
                'company.edit',
                compact('company')
            );
    }

    /**
     * @param \App\Http\Requests\Company $request
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Company $request, \App\Company $company)
    {
        if ($company->update($request->all())) {

            $response =
                redirect()
                    ->route(
                        'company.show',
                        $company,
                        302,
                        [
                            'X-Updated' => 1,
                        ]
                    )
                    ->with('success', 'Company updated');

            if (
                $request->hasFile('logo')
                && !CompanyLogo::processLogo(
                    $company,
                    $request->file('logo')->getRealPath(),
                    $request->file('logo')->getClientOriginalExtension()
                )
            ) {
                $response->with('warning', 'Logo not updated');
            }

            return $response;
        }

        return
            redirect()
                ->route('company.edit', $company)
                ->with('error', 'Company not updated');
    }

    /**
     * @param \App\Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Company $company)
    {
        if ($company->delete()) {
            return
                redirect()
                    ->route(
                        'company.index',
                        [],
                        302,
                        [
                            'X-Deleted' => 1,
                        ]
                    )
                    ->with('success', 'Company deleted');
        }

        return
            redirect()
                ->route('company.show', $company)
                ->with('error', 'Company not deleted');
    }
}
