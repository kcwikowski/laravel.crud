<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Controller
{

    public function login()
    {

        return
            response()->view('login');
    }

    public function logout()
    {
        Auth::logout();

        return
            redirect()->route('login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {

            return
                redirect()
                    ->intended(
                        '/',
                        302,
                        [
                            'X-Authenticated' => 1,
                        ]
                    );
        }

        return
            redirect()
                ->route('login')
                ->with('error', 'Invalid email or password');
    }
}
