<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class Company extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
            [
                'name'    => 'required|string|min:3|max:255|unique:company',
                'email'   => 'required|email|unique:company',
                'website' => 'required|url|unique:company',
                'logo'    => 'image|mimes:jpeg,jpg,png',
            ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->flash();
        parent::failedValidation($validator);
    }
}