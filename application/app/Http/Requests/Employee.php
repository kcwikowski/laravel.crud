<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class Employee extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
            [
                'company_id' => 'required|exists:company,id',
                'first_name' => 'required|string|min:3|max:255',
                'last_name'  => 'required|string|min:3|max:255',
                'email'      => 'email|unique:employee',
                'phone'      => 'required|min:3|max:255',
            ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->flash();
        parent::failedValidation($validator);
    }
}