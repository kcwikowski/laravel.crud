@extends('layouts.master')

@section('title', 'Company details')
@section('content')
    <div class="container-fluid pt-3">
        <div class="row">
            <div class="col-md-12">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid" src="{{\App\Http\Middleware\CompanyLogo::getLogoUrl($company)}}"
                                 alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{$company->name}}</h3>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Email</b> <a class="float-right">{{$company->email}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Website</b> <a class="float-right">{{$company->website}}</a>
                            </li>
                        </ul>

                        <form action="{{route('company.destroy',$company->id)}}" method="POST" class="text-center">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('company.edit',$company->id)}}" class="btn btn-primary">
                                Edit
                            </a>
                            <button type="submit" class="btn btn-danger">
                               Delete
                            </button>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->

                <!-- /.card -->
            </div>
            <!-- /.col -->

            <!-- /.col -->
        </div>
    </div>
@stop