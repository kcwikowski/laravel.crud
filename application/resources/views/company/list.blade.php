<div class="container-fluid pt-3">
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Companies</h3>
            <div class="card-tools">
                <a href="{{route('company.create')}}" class="btn btn-tool btn-sm">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Website</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $company)
                    <tr>
                        <td>
                            <a href="{{route('company.show',$company->id)}}">
                                <img src="{{\App\Http\Middleware\CompanyLogo::getLogoThumbnailUrl($company)}}"
                                     alt="Product 1"
                                     class="img-size-32 mr-2">
                                {{$company->name}}
                            </a>
                        </td>
                        <td>
                            {{$company->website}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if($paginate)
                <div class="card-header border-0">
                    <div class="card-tools">
                        {{$data->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
