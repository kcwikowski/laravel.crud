@extends('layouts.master')

@section('title', 'Companies list')
@section('content')
    @include('company.list',['paginate'=>true])
@stop