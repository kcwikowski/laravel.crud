@extends('layouts.master')

@section('title', $company->exists ? 'Edit company':'Create new company')
@section('content')
    <div class="container-fluid pt-3">
        <div class="card">
            <form method="POST"
                  action="{{ $company->exists ? route('company.update',$company):route('company.store') }}"
                  enctype="multipart/form-data">
                @if($company->exists)
                    @method('PUT')
                @endif
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" id="name" placeholder="Enter company name" name="name"
                               value="{{old('name',$company->name)}}">
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"
                               value="{{old('email',$company->email)}}">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="website">Website</label>
                        <input class="form-control" id="website" placeholder="Enter website URL" name="website"
                               value="{{old('website',$company->website)}}">
                        @error('website')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="logo">Logo</label>
                        <div class="custom-file">
                            <input type="file" id="logo" name="logo" accept="image/*">
                            <label class="custom-file-label" for="logo">Choose file</label>
                        </div>
                        @error('logo')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@stop