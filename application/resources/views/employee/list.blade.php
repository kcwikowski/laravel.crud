<div class="container-fluid pt-3">
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Employees</h3>
            <div class="card-tools">
                <a href="{{route('employee.create')}}" class="btn btn-tool btn-sm">
                    <i class="fa fa-user-plus"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Company</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $employee)
                    <tr>
                        <td>
                            <a href="{{route('employee.show',$employee->id)}}">
                                {{$employee->first_name}} {{$employee->last_name}}
                            </a>
                        </td>
                        <td>
                            <a href="{{route('company.show',$employee->company()->getResults()->id)}}">
                                {{$employee->company()->getResults()->name}}
                            </a>
                        </td>
                        <td>
                            {{$employee->email}}
                        </td>
                        <td>
                            {{$employee->phone}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @if($paginate)
                <div class="card-header border-0">
                    <div class="card-tools">
                        {{$data->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
