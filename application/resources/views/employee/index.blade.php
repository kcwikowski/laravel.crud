@extends('layouts.master')

@section('title', 'Employees list')
@section('content')
    @include('employee.list',['paginate'=>true])
@stop