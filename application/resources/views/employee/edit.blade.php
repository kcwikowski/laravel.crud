@extends('layouts.master')
@section('title', $employee->exists ? 'Edit employee':'Create new employee')

@section('content')
    <div class="container-fluid pt-3">
        <div class="card">
            <form method="POST"
                  action="{{ $employee->exists ? route('employee.update',$employee):route('employee.store') }}">

                @if($employee->exists)
                    @method('PUT')
                @endif
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="company">Company</label>
                        <input class="form-control" id="company" placeholder="Enter company" name="company"
                               value="{{old('company',$employee->company()->getResults()?$employee->company()->getResults()->name:'')}}">
                        <input id="companyId" type="hidden" name="company_id"
                               value="{{old('company_id',$employee->company()->getResults()?$employee->company()->getResults()->id:'')}}">
                        @error('company_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="firstName">First name</label>
                        <input class="form-control" id="firstName" placeholder="Enter first name" name="first_name"
                               value="{{old('first_name',$employee->first_name)}}">
                        @error('first_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="lastName">Last name</label>
                        <input class="form-control" id="lastName" placeholder="Enter last name" name="last_name"
                               value="{{old('last_name',$employee->last_name)}}">
                        @error('last_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"
                               value="{{old('email',$employee->email)}}">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input class="form-control" id="phone" placeholder="Enter phone" name="phone"
                               value="{{old('phone',$employee->phone)}}">
                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    @include('company.autocomplete',['source'=>'#company','destination'=>'#companyId'])
@stop