@extends('layouts.master')

@section('title', 'Employee details')
@section('content')
    <div class="container-fluid pt-3">
        <div class="row">
            <div class="col-md-12">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="../../dist/img/avatar.png"
                                 alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">{{$employee->first_name}} {{$employee->last_name}}</h3>

                        <p class="text-muted text-center">{{$employee->company()->getResults()->name}}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Email</b> <a class="float-right">{{$employee->email}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Phone</b> <a class="float-right">  {{$employee->phone}}</a>
                            </li>
                        </ul>

                        <form action="{{route('employee.destroy',$employee->id)}}" method="POST" class="text-center">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('employee.edit',$employee->id)}}" class="btn btn-primary">
                                Edit
                            </a>
                            <button type="submit" class="btn btn-danger">
                               Delete
                            </button>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->

                <!-- /.card -->
            </div>
            <!-- /.col -->

            <!-- /.col -->
        </div>
    </div>
@stop