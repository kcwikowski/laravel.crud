@extends('layouts.master')

@section('title', 'Dashboard')
@section('content')
    @include('company.list',['data'=>$companies,'paginate'=>false])
    @include('employee.list',['data'=>$employees,'paginate'=>false])
@stop