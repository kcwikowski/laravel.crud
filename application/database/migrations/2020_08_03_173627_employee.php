<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table
                ->string('id', 36)
                ->unique()
                ->primary();
            $table->string('first_name');
            $table->string('last_name');
            $table
                ->string('email')
                ->unique();
            $table
                ->string('phone')
                ->unique();
            $table->string('company_id');
            $table->timestamps();
            $table
                ->foreign('company_id')
                ->references('id')->on('company')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
