<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new \App\User())
            ->setAttribute('id', \Ramsey\Uuid\Uuid::uuid4()->toString())
            ->setAttribute('name', 'admin')
            ->setAttribute('email', 'admin@admin.com')
            ->setAttribute('email_verified_at', now())
            ->setAttribute('remember_token', \Illuminate\Support\Str::random(16))
            ->setAttribute('password', Hash::make('password'))
            ->save();
    }
}
