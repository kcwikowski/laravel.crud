<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c         = rand(15, 25);
        $faker     = \Faker\Factory::create();
        $companies = [];
        $this->command->getOutput()->write("<comment>Creating DB entries:</comment>{$c}");
        for ($i = 0; $i < $c; $i++) {
            ($companies[] = new \App\Company())
                ->setAttribute('name', $faker->unique(false, 5)->company)
                ->setAttribute('email', $faker->unique(false, 5)->companyEmail)
                ->setAttribute('website', $faker->unique(false, 5)->url)
                ->save();
            if ($i % 10 === 0) {
                $this->command->getOutput()->write(".");
            }
        }
        $this->command->getOutput()->writeln("");
        $this->seedCompanyLogo($companies);
    }

    /**
     * @param \App\Company|\App\Company[] $companies
     */
    protected function seedCompanyLogo($companies)
    {
        $this->command->getOutput()->write("<comment>Creating logos:</comment>");

        $sampleLogos =
            \Illuminate\Support\Facades\File::files(
                implode(
                    DIRECTORY_SEPARATOR,
                    [__DIR__, 'Company', 'logo']
                )
            );

        $c = 0;
        foreach (Arr::wrap($companies) as $company) {
            /**
             * @var \Symfony\Component\Finder\SplFileInfo $logo
             * @var \App\Company $company
             */
            $logo = Arr::random($sampleLogos);

            if (!\App\Http\Middleware\CompanyLogo::processLogo($company, $logo->getRealPath())) {
                $this->command->getOutput()->write("<error>.</error>");
            }
            if ($c++ % 5 === 0) {
                $this->command->getOutput()->write(".");
            }
        }
        $this->command->getOutput()->writeln("");
    }
}
