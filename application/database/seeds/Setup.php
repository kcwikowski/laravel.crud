<?php

use Illuminate\Database\Seeder;

class Setup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(
            [
                CompanySeeder::class,
                EmployeeSeeder::class,
            ]
        );
    }
}
