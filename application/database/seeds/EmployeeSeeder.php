<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->getOutput()->writeln("<comment>Fetching company data...</comment>");
        $companies = \App\Company::all();
        if (!$companies->count()) {
            $this->command->getOutput()->writeln("<warning>No companies found</warning>");

            return;
        }
        $this->command->getOutput()->writeln("<info>Companies found:</info>" . $companies->count());
        $c     = $companies->count() * rand(5,10);
        $faker = \Faker\Factory::create();
        $this->command->getOutput()->write("<comment>Creating DB entries:</comment>{$c}");
        for ($i = 0; $i < $c; $i++) {
           try {
               (new \App\Employee())
                   ->setAttribute('first_name', $faker->firstName)
                   ->setAttribute('last_name', $faker->lastName)
                   ->setAttribute('email', $faker->unique(false, 5)->email)
                   ->setAttribute('phone', $faker->unique(false, 5)->phoneNumber)
                   ->setAttribute('company_id', $companies->random()->id)
                   ->save();
           }catch (Exception $e){
               //In case of email duplicates
               $this->command->getOutput()->write("<error>.</error>");
           }
            if ($i % 5 === 0) {
                $this->command->getOutput()->write(".");
            }
        }
        $this->command->getOutput()->writeln("");
    }
}
