<?php

namespace Tests;

use App\User;

abstract class AbstractAuthTestCase extends TestCase
{
    use CreatesApplication;

    protected function initActor()
    {
        $user =
            User::query()
                ->where('name', 'admin')
                ->first()
                ->get()
                ->first();
        return
            $this->actingAs($user);
    }
}
