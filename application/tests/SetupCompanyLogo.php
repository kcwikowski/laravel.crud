<?php

namespace Tests;

use App\Http\Middleware\CompanyLogo;

class SetupCompanyLogo
{
    CONST ORIGINAL_PREFIX  = 'resources/original_test/';
    CONST THUMBNAIL_PREFIX = 'resources/thumbnail_test/';

    public static function setUp()
    {
        CompanyLogo::setThumbnailPrefix(self::THUMBNAIL_PREFIX);
        CompanyLogo::setOriginalPrefix(self::ORIGINAL_PREFIX);
    }
}
