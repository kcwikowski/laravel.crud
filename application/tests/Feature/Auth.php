<?php

namespace Tests\Feature;

use Tests\AbstractAuthTestCase;

class Auth extends AbstractAuthTestCase
{
    const TEST_CREDENTIALS = [
        'email'    => 'admin@admin.com',
        'password' => 'password',
    ];

    public function testRedirect()
    {
        $this->assertGuest();

        $this
            ->get('/')
            ->assertStatus(302);
    }

    public function testLoginPage()
    {
        $this
            ->get('/login')
            ->assertStatus(200);
    }

    public function testCreateUser()
    {
        $faker = \Faker\Factory::create();

        $this
            ->assertTrue(
                (new \App\User())
                    ->setAttribute('name', $faker->name)
                    ->setAttribute('email', $faker->email)
                    ->setAttribute('email_verified_at', now())
                    ->setAttribute('remember_token', \Illuminate\Support\Str::random(16))
                    ->setAttribute('password', \Illuminate\Support\Str::random(8))
                    ->save()
            );
    }

    public function testAuth()
    {
        $this
            ->initActor()
            ->get('/')
            ->assertStatus(200);
    }

    public function testValidCredentials()
    {
        $this
            ->post('/login', self::TEST_CREDENTIALS)
            ->assertStatus(302)
            ->assertHeader('X-Authenticated', 1);
    }

    public function testInvalidCredentials()
    {

        $this
            ->post('/login', [])
            ->assertStatus(302)
            ->assertHeaderMissing('X-Authenticated');
    }

    public function testLogout()
    {
        $this
            ->initActor()
            ->get('/logout')
            ->assertStatus(302);
    }
}
