<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Ramsey\Uuid\Uuid;
use Tests\AbstractAuthTestCase;
use App\Company;
use Tests\SetupCompanyLogo;

class CompanyLogo extends AbstractAuthTestCase
{
    public function testLogoUrl()
    {
        ($company = new Company())
            ->setAttribute('logo', 'test.jpg');
        $url = \App\Http\Middleware\CompanyLogo::getLogoUrl($company, false);
        $this->assertEquals(
            $url,
            SetupCompanyLogo::ORIGINAL_PREFIX . $company->logo
        );
    }

    public function testLogoThumbnailUrl()
    {
        ($company = new Company())
            ->setAttribute('logo', 'test.jpg');
        $url = \App\Http\Middleware\CompanyLogo::getLogoThumbnailUrl($company, false);
        $this->assertEquals(
            $url,
            SetupCompanyLogo::THUMBNAIL_PREFIX . $company->logo
        );
    }

    public function testInvalidFile()
    {
        $file = UploadedFile::fake()->create('not_an_image.png', "some text");
        $this->assertFalse(
            \App\Http\Middleware\CompanyLogo::processLogo(
                new Company(),
                $file->getRealPath()
            )
        );
    }

    public function testInvalidPath()
    {
        $this->assertFalse(
            \App\Http\Middleware\CompanyLogo::processLogo(
                new Company(),
                Uuid::uuid4()->toString()
            )
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        SetupCompanyLogo::setUp();
    }
}
