<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Ramsey\Uuid\Uuid;
use Tests\AbstractAuthTestCase;
use Tests\SetupCompanyLogo;

class Company extends AbstractAuthTestCase
{
    public function testAuth()
    {
        $this
            ->get('/company')
            ->assertStatus(302);
    }

    public function testIndex()
    {
        $this
            ->initActor()
            ->get('/company')
            ->assertStatus(200);
    }

    public function testCreate()
    {
        $this
            ->initActor()
            ->get('/company/create')
            ->assertStatus(200)
            ->assertViewIs('company.edit');
    }

    public function testStore()
    {
        $faker = \Faker\Factory::create();
        $file  = UploadedFile::fake()->image('logo.jpg', 200, 200);
        $this
            ->initActor()
            ->post(
                '/company',
                [
                    'name'    => $faker->company,
                    'email'   => $faker->companyEmail,
                    'website' => $faker->url,
                    'logo'    => $file,
                ])
            ->assertStatus(302)
            ->assertHeader('X-Created', 1);
    }

    public function testStoreInvalid()
    {
        $file = UploadedFile::fake()->image('logo.jpg', 200, 200);
        $this
            ->initActor()
            ->post(
                '/company',
                [
                    'logo' => $file,
                ])
            ->assertStatus(302)
            ->assertHeaderMissing('X-Created');
    }

    public function testShow()
    {
        $this->initActor();

        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();
        $this
            ->get('/company/' . $company->id)
            ->assertStatus(200);
    }

    public function testShowInvalid()
    {
        $this->initActor();

        $this
            ->get('/company/' . Uuid::uuid4()->toString())
            ->assertStatus(404);
    }

    public function testEdit()
    {
        $this->initActor();

        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();
        $this
            ->get('/company/' . $company->id . '/edit')
            ->assertStatus(200);
    }

    public function testEditInvalid()
    {
        $this->initActor();

        $this
            ->get('/company/' . Uuid::uuid4()->toString() . '/edit')
            ->assertStatus(404);
    }

    public function testUpdate()
    {
        $this->initActor();

        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();

        $faker = \Faker\Factory::create();
        $file  = UploadedFile::fake()->image('logo.jpg', 200, 200);

        $this
            ->initActor()
            ->put(
                '/company/' . $company->id,
                [
                    'name'    => $faker->company,
                    'email'   => $faker->companyEmail,
                    'website' => $faker->url,
                    'logo'    => $file,
                ])
            ->assertStatus(302)
            ->assertHeader('X-Updated', 1);
    }

    public function testUpdateWithInvalidLogo()
    {
        $this->initActor();

        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();

        $faker = \Faker\Factory::create();
        $file  = UploadedFile::fake()->create('logo.jpg', "some text");

        $this
            ->initActor()
            ->put(
                '/company/' . $company->id,
                [
                    'name'    => $faker->company,
                    'email'   => $faker->companyEmail,
                    'website' => $faker->url,
                    'logo'    => $file,
                ])
            ->assertStatus(302)
            ->assertHeader('X-Updated', 1);
    }

    public function testAutocomplete()
    {
        $this->initActor();

        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();
        $name    = substr($company->name, 0, 2);
        $this
            ->post(
                '/company/autocomplete',
                [
                    'name' => $name,
                ])
            ->assertStatus(200)
            ->assertSeeText($name);
    }

    public function testDestroy()
    {
        $this->initActor();

        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();
        $this
            ->delete('/company/' . $company->id)
            ->assertStatus(302)
            ->assertHeader('X-Deleted', 1);
    }

    public function testDestroyInvalid()
    {
        $this->initActor();

        $this
            ->delete('/company/' . Uuid::uuid4()->toString())
            ->assertStatus(404)
            ->assertHeaderMissing('X-Deleted');
    }

    protected function setUp(): void
    {
        parent::setUp();
        SetupCompanyLogo::setUp();
    }
}
