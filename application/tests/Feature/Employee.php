<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Ramsey\Uuid\Uuid;
use Tests\AbstractAuthTestCase;

class Employee extends AbstractAuthTestCase
{
    public function testAuth()
    {
        $this
            ->get('/employee')
            ->assertStatus(302);
    }

    public function testIndex()
    {
        $this
            ->initActor()
            ->get('/employee')
            ->assertStatus(200);
    }

    public function testCreate()
    {
        $this
            ->initActor()
            ->get('/employee/create')
            ->assertStatus(200)
            ->assertViewIs('employee.edit');
    }

    public function testStore()
    {
        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();

        $faker = \Faker\Factory::create();

        $this
            ->initActor()
            ->post(
                '/employee',
                [
                    'company_id' => $company->id,
                    'first_name' => $faker->firstName,
                    'last_name'  => $faker->lastName,
                    'email'      => $faker->email,
                    'phone'      => $faker->phoneNumber,
                ])
            ->assertStatus(302)
            ->assertHeader('X-Created', 1);
    }

    public function testStoreInvalid()
    {
        $this
            ->initActor()
            ->post('/employee', [])
            ->assertStatus(302)
            ->assertHeaderMissing('X-Created');
    }

    public function testShow()
    {
        $this->initActor();

        /**
         * @var \App\Employee $employee
         */
        $employee =
            \App\Employee::query()
                ->first()
                ->get()
                ->first();
        $this
            ->get('/employee/' . $employee->id)
            ->assertStatus(200);
    }

    public function testShowInvalid()
    {
        $this->initActor();

        $this
            ->get('/employee/' . Uuid::uuid4()->toString())
            ->assertStatus(404);
    }

    public function testEdit()
    {
        $this->initActor();

        /**
         * @var \App\Employee $employee
         */
        $employee =
            \App\Employee::query()
                ->first()
                ->get()
                ->first();
        $this
            ->get('/employee/' . $employee->id . '/edit')
            ->assertStatus(200);
    }

    public function testEditInvalid()
    {
        $this->initActor();

        $this
            ->get('/employee/' . Uuid::uuid4()->toString() . '/edit')
            ->assertStatus(404);
    }

    public function testUpdate()
    {
        $this->initActor();
        /**
         * @var \App\Employee $employee
         */
        $employee =
            \App\Employee::query()
                ->first()
                ->get()
                ->first();
        /**
         * @var \App\Company $company
         */
        $company =
            \App\Company::query()
                ->first()
                ->get()
                ->first();

        $faker = \Faker\Factory::create();

        $this
            ->initActor()
            ->put(
                '/employee/' . $employee->id,
                [
                    'company_id' => $company->id,
                    'first_name' => $faker->firstName,
                    'last_name'  => $faker->lastName,
                    'email'      => $faker->email,
                    'phone'      => $faker->phoneNumber,
                ])
            ->assertStatus(302)
            ->assertHeader('X-Updated', 1);
    }

    public function testDestroy()
    {
        $this->initActor();

        /**
         * @var \App\Employee $employee
         */
        $employee =
            \App\Employee::query()
                ->first()
                ->get()
                ->first();

        $this
            ->delete('/employee/' . $employee->id)
            ->assertStatus(302)
            ->assertHeader('X-Deleted', 1);
    }

    public function testDestroyInvalid()
    {
        $this->initActor();

        $this
            ->delete('/employee/' . Uuid::uuid4()->toString())
            ->assertStatus(404)
            ->assertHeaderMissing('X-Deleted');
    }
}
