#!/bin/bash

# ---------------------------------------
# Variables
# ---------------------------------------
APP_NAME="cyber_duck"
PROJECT_DIR=$(realpath ../)

# ---------------------------------------
# Kill existing container if existing
# ---------------------------------------
EXISTING_CONTAINER=`docker ps -af name=${APP_NAME} -q`

if [ ! -z "${EXISTING_CONTAINER}" ]
    then
        echo "Killing existing container"
        docker rm -f ${EXISTING_CONTAINER}
fi

# ---------------------------------------
# Pull needed images
# ---------------------------------------
docker pull mattrayner/lamp

# ---------------------------------------
# Start containers
# ---------------------------------------
# Application
echo "Warming up..."
docker run --name ${APP_NAME} -p 8000:80 -p 3306:3306 -w /var/www/html/ \
-v ${PROJECT_DIR}/docker/container-init.sh:/bin/container-init.sh \
-v ${PROJECT_DIR}/application/:/var/www/html/ \
-v ${PROJECT_DIR}/docker/config/apache.conf:/etc/apache2/sites-available/${APP_NAME}.conf \
-d mattrayner/lamp
echo "Initializing..."
docker exec -it ${APP_NAME} bash /bin/container-init.sh
echo "Connecting"
docker exec -it ${APP_NAME} bash