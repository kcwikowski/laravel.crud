#!/bin/bash

# ------------------------------------------------
# Variables
# ------------------------------------------------
APP_NAME="cyber_duck"
LOCK_FILE="container.lock"
DATE_TIMEZONE=Europe/London
if [ -d ${LOCK_FILE} ]; then
    echo "Other container-init process is active"
    exit 1
fi
date > ${LOCK_FILE}
# ------------------------------------------------
# Managing needed modules
# ------------------------------------------------
apt-get install -y php-imagick

# ------------------------------------------------
# Prepare php.ini
# ------------------------------------------------
sed -i 's|^;date.timezone =|date.timezone = '${DATE_TIMEZONE}'|' /etc/php/7.4/apache2/php.ini
sed -i 's|^;date.timezone =|date.timezone = '${DATE_TIMEZONE}'|' /etc/php/7.4/cli/php.ini

# ------------------------------------------------
# Start MySQL
# ------------------------------------------------
service mysql restart

# ------------------------------------------------
# Create users & grant permissions
# ------------------------------------------------
mysql -e "DROP DATABASE IF EXISTS ${APP_NAME}"
mysql -e "DROP DATABASE IF EXISTS ${APP_NAME}_test"
mysql -e "DROP USER IF EXISTS ${APP_NAME}"
mysql -e "FLUSH PRIVILEGES;"
mysqladmin flush-privileges
mysql -e "CREATE DATABASE ${APP_NAME} CHARACTER SET utf8 COLLATE utf8_bin"
mysql -e "CREATE DATABASE ${APP_NAME}_test CHARACTER SET utf8 COLLATE utf8_bin"
mysql -e "CREATE USER ${APP_NAME}@'%' IDENTIFIED BY PASSWORD '*EDF0DBFCA8958ED8E89C7567801177A5A50761E6';" #h5jIN3h2haG46E5aq78uF13o4aJik5
mysql -e "GRANT ALL ON ${APP_NAME}.* TO ${APP_NAME}@'%';"
mysql -e "GRANT ALL ON ${APP_NAME}_test.* TO ${APP_NAME}@'%';"
mysql -e "FLUSH PRIVILEGES;"

# ------------------------------------------------
# Prepare project
# ------------------------------------------------
composer install --no-interaction
# ------------------------------------------------
# Restart Apache service
# ------------------------------------------------
a2ensite ${APP_NAME}
service apache2 reload

# ------------------------------------------------
echo Initialization finished
rm -rf ${LOCK_FILE}