#!/bin/bash

# ---------------------------------------
# Variables
# ---------------------------------------
APP_NAME="cyber_duck"

# ---------------------------------------
# Kill existing container if existing
# ---------------------------------------
EXISTING_CONTAINER=`docker ps -af name=${APP_NAME} -q`

if [ -z "${EXISTING_CONTAINER}" ]
    then
        echo "Container not running"
        exit
fi

# ---------------------------------------
# Start containers
# ---------------------------------------
# Application
echo "Connecting"
docker exec -it ${APP_NAME} bash