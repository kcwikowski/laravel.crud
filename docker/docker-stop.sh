#!/bin/bash

# ---------------------------------------
# Variables
# ---------------------------------------
APP_NAME="cyber_duck"
PROJECT_DIR=$(realpath ../)

# ---------------------------------------
# Kill existing container if existing
# ---------------------------------------
EXISTING_CONTAINER=`docker ps -af name=${APP_NAME} -q`

if [ ! -z "${EXISTING_CONTAINER}" ]
    then
        echo "Killing existing container"
        docker rm -f ${EXISTING_CONTAINER}
fi